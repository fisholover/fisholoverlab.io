---
layout: post
title:  "Elephant"
date:   2017-11-28 12:36:38 +0800
tag: [general]
image: "article/elephants_wide.jpg"
hero:
    theme: secondary
    image:
        wide: "article/elephants_wide.jpg"
        regular: "article/elephants_normal.jpg"
        narrow: "article/elephants_narrow.png"
    title: The Year's Best Animal Photos
    author: J. T. Lee
    date: January 18th, 2017
carousel:
    -   img: article/zebras.jpg
        title: Zebras grazing.
        author: Anna Q.
    -   img: article/sheep.jpg
        title: Sheep in a field.
        author: L. Fischer
    -   img: article/camels.jpg
        title: Camels on a beach.
        author: Mali D.
---
Vivamus viverra augue libero, vitae dapibus lectus accumsan eget. Pellentesque eget ipsum purus. Maecenas leo odio, ornare nec ex id, suscipit porta ipsum. Ut fringilla semper cursus.

Donec porta orci eu tortor finibus, id facilisis metus aliquam. Pellentesque lobortis viverra odio ut eleifend. Integer elementum finibus magna, consequat fermentum dui ultrices sit amet.

Sed leo sapien, molestie sit amet lorem eu, suscipit imperdiet tortor. Mauris maximus magna quam, non sodales metus auctor nec. Aenean tristique massa enim, non dictum mauris eleifend tristique. Proin fermentum nulla a nulla bibendum ultricies. Nulla pulvinar, risus vel tristique aliquet, elit quam tincidunt nisi, non blandit leo nulla eu ipsum. Sed porta, felis vitae elementum pellentesque, mauris felis rhoncus quam, ac suscipit eros justo ac justo. Proin et elit vitae sem interdum posuere et vitae nibh. Ut sed orci aliquam, pulvinar felis ac, pretium massa. Nullam porta ipsum non euismod mollis. Quisque scelerisque nisi quis pharetra blandit.

Vestibulum eu varius dolor. Praesent sagittis magna sem, non bibendum quam aliquam et. Sed et tristique mi. Quisque porta lorem et nulla lacinia gravida. Nullam semper lobortis sem, interdum tempus tellus. Proin accumsan imperdiet leo at vulputate. Nulla euismod placerat finibus. Lorem ipsum dolor sitamet, consectetur adipiscing elit.

Aenean ante erat, egestas a justo eu, sollicitudin convallis nulla. Fusce ex est, ornare in odio eu, venenatis dapibus purus. Etiam sit amet orci quam. Proin lobortis lobortis tellus, non elementum nunc consequat ac. Suspendisse eu purus sit amet dolor elementum mattis ut in odio.

{% include image/caption.html image="article/gorilla.png" description="Description of the photograph." author="D.F. Roy" link="https://ampstart.com/templates/article/article.amp" %}

{% include image/carousel.html data=page.carousel %}

Vestibulum eu varius dolor. Praesent sagittis magna sem, non bibendum quam aliquam et. Sed et tristique mi. Quisque porta lorem et nulla lacinia gravida. Nullam semper lobortis sem, interdum tempus tellus. Proin accumsan imperdiet leo at vulputate. Nulla euismod placerat finibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Mauris aliquet eleifend lacinia. Maecenas non finibus ante, et maximus urna. In non enim a lectus molestie tincidunt. Curabitur eu tortor odio. Aliquam bibendum elit risus, vitae rutrum ex placerat eget.

{% include sns/instagram.html shortcode="BDX4-nClRVw" %}

Integer fringilla eu lacus sit amet tristique. Nulla sagittis fermentum porta. Aenean convallis nulla eu nulla lobortis, sit amet sagittis mauris tincidunt. Praesent sed efficitur odio. Quisque et sollicitudin nisi. Fusce sed auctor ligula. Suspendisse potenti. Nam hendrerit sollicitudin enim, non aliquet lacus congue et.
