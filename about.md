---
layout: page
title: About
permalink: /about/
---

Fisholover is the go-to source for fish related information, sustainable aquaculture and fisheries resource content. 

We are dedicated team who have a vision to deliver interesting topics that inspired us so that you will get in-depth stories that matters to you.

Here at fisholover.com, we are also educators who believe that everyone deserve to learn. We have created several online courses related to aquaculture in Udemy.com that you can enrol and learn at your own pace, flexible time.